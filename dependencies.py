from fastapi.security import APIKeyHeader
from fastapi import Depends, HTTPException, status, Security

header_scheme = APIKeyHeader(name="X-API-KEY")
def very_apikey(key: str = Depends(header_scheme)):
    print(key)
    if key != "toto" :
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Authenfication Key error",
        )