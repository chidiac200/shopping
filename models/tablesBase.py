import datetime

from sqlmodel import SQLModel, Field
from pydantic import EmailStr, SecretStr, HttpUrl


class CustomersBase(SQLModel):
    first_name: str
    last_name: str
    email: EmailStr
    password: str
    avatar: str


class Customer_addressesBase(SQLModel):
    address: str
    address2: str
    city: str
    state: str
    zipcode: str
    customer: int = Field(foreign_key="customers.id", description="Field is foreign_key references table customers")


class Product_categoriesBase(SQLModel):
    name: str
    description: str


class ProductsBase(SQLModel):
    name : str
    description : str = Field(description="describe the prouct", default="toto")
    price : str
    category : int = Field(foreign_key="product_categories.id")
    image : str



class OrdersBase(SQLModel):
    customer : int = Field(foreign_key="customers.id", description="Field is foreign_key references table customers")
    product: int = Field(foreign_key="products.id", description="Field is foreign_key references table Products")


class ReviewsBase(SQLModel):
    title: str
    body: str
    rating :int
    customer : int = Field(foreign_key="customers.id", description="Field is foreign_key references table customers")
    product : int = Field(foreign_key="products.id",
                          description="Field is foreign_key reference product_categorie table")
    #review_date: datetime.datetime
