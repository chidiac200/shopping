from models.tablesBase import *
from typing import Optional
from sqlmodel import Relationship
from typing import List


class Customers(CustomersBase, table=True):
    id : Optional[int] = Field(default=None, primary_key=True, description= "This is primary key")
    #join_date: datetime



class Customer_addresses(Customer_addressesBase , table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class Orders(OrdersBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class Products(ProductsBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, description= "This is primary key")
    #create_date: Optional[datetime] = Field(default=datetime)
    product_catgorie: Optional["Product_categories"] = Relationship(back_populates="productss")


class Product_categories(Product_categoriesBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    productss : List[Products] = Relationship(back_populates="product_catgorie")


class Reviews(ReviewsBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
