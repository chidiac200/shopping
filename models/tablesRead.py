from models.tablesBase import *
from sqlmodel import SQLModel


from typing import Optional
from models.tablesBase import *
class CustomersRead(CustomersBase):
    id : int
    #join_date: datetime


class Customer_addressesRead(Customer_addressesBase):
    id : int


class OrdersRead(OrdersBase):
    id : int


class ProductsRead(ProductsBase):
    id: int
    #create_date: Optional[datetime] = Field(default=datetime)


class Product_categoriesRead(Product_categoriesBase):
    id: int


class Reviews(ReviewsBase):
    id: int
