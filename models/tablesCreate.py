from models.tablesBase import *


class CustomersCreate(CustomersBase):
    pass


class Customer_addressesCreate(Customer_addressesBase):
    pass


class OrdersCreate(OrdersBase):
    pass


class ProductsCreate(ProductsBase):
    pass


class Product_categoriesCreate(Product_categoriesBase):
    pass


class ReviewsCreate(ReviewsBase):
    pass
