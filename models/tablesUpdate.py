from sqlmodel import SQLModel


class CustomersUpdate(SQLModel, table=True):
    pass


class Customer_addressesUpdate(SQLModel, table=True):
    pass


class OrdersUpdate(SQLModel, table=True):
    pass


class ProductsUpdate(SQLModel, table=True):
    pass


class Product_categoriesUpdate(SQLModel, table=True):
    pass


class ReviewsUpdate(SQLModel, table=True):
    pass
