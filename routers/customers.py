from fastapi import APIRouter, Security, Body
from models.tablesCreate import CustomersCreate
from models.tablesRead import CustomersRead
from dependencies import very_apikey
from typing import Annotated


router = APIRouter(
    prefix= "/customers",
    tags= ["Customers"],
    dependencies = [
        Security(very_apikey)
    ],
)

#this endpoint add a new customer
@router.post("/add")
async def add_one_customer(customer : CustomersCreate) -> CustomersRead:
    return customer

