from fastapi import APIRouter, Security, Depends, Form, UploadFile, File, HTTPException
from models.maintables import Products, Product_categories
from models.tablesCreate import ProductsCreate, Product_categoriesCreate
from models.tablesRead import ProductsRead, Product_categoriesRead
from models.relationtable import *
from dependencies import very_apikey
from sqlmodel import Session
from typing import List, Annotated
from database.connexion import get_session
from database.crud.create import Create
from pydantic import BaseModel
import secrets


"""
GLOBAL VAR
"""
host ="http://localhost:8000"

scheme_api = Security(very_apikey)
router = APIRouter(
    prefix= "/product",
    tags= ["Product"],
    dependencies = [
        Security(very_apikey)
    ]
)

"""
This section have other function that directory will changer after
"""
async def save_image(image: UploadFile, directory, filname):
    format_accept = ["png", "jpeg"]
    format_file = image.content_type.split("/")[1]
    if format_file in format_accept:
        data = await image.read()
        with open(f"{directory}/{filname}.{format_file}", "wb") as savefile :
            savefile.write(data)
            savefile.close()






#add categorie product in DB
@router.post("/addcategory")
async def add_category_product(category: Product_categoriesCreate, session: Session = Depends(get_session)) -> Product_categoriesRead:
    new_category : Product_categoriesRead = Create.add_in_DB(session, Product_categories, category)
    return new_category



#add product in DB
@router.post("/addproduct")
async def add_one_product( name : Annotated[str, Form()],
                           description: Annotated[str, Form()],
                           price: Annotated[str, Form()],
                           category: Annotated[int, Form()],
                           image : Annotated[UploadFile, File(description="ajout image") ],
                            session: Session = Depends(get_session) ) -> ProductsRead:
    file_name = secrets.token_hex(16)
    await save_image(image, "static/images", file_name)
    data_form = ProductsCreate(name=name, description=description, price=price, category= category, image=f"static/images/{file_name}.{image.content_type.split('/')[1]}")
    """
    ce point permet d'ajoute un product dans la DB
    """
    new_client: ProductsRead = Create.add_in_DB(session, Products, data_form )
    new_client.image = f"{host}/static/images/{file_name}.{image.content_type.split('/')[1]}"
    return new_client


#read all data in DB
@router.get("/readall")
async def read_all_data (session: Session = Depends(get_session)) -> List[ProductsRead]:
    """
    This endpoint reuturn all data in List[ProductRead]
    EX: in Typscript the data response is Map[ProductRead]
    """
    all_data  = Create.read_all_data_table(session, Products)

    return all_data

@router.get("/{product_id}", response_model= ProductReadWithCategory)
async def product_with_id(product_id: int, session: Session= Depends(get_session)):
    product = Create.read_by_id(session, Products, product_id)
    return product

@router.get("/categorie/{categorie_id}", response_model= CategorieReadWithProduct)
async def categorie_with_id(categorie_id: int, session: Session= Depends(get_session)):
    categorie_with_product = Create.read_by_id(session, Product_categories, categorie_id)
    return categorie_with_product