import sqlalchemy.exc
from sqlmodel import  SQLModel, create_engine, Session
from fastapi import HTTPException, status

def enginedb(dbuser : str, dbname: str, dbpassword: str = "root", dbhost: str = "127.0.0.1", dbport: int = 3306):
    dburl: str = f"mysql://{dbuser}:{dbpassword}@{dbhost}:{dbport}/{dbname}"
    engine = create_engine(dburl, pool_timeout=10, echo=True)
    return engine

def create_db_and_tables():
    try:
        SQLModel.metadata.create_all(enginedb('root','apiproduct'))
    except sqlalchemy.exc.OperationalError as e:
        return {"db": "db"}

def get_session ():
    with Session (enginedb('root','apiproduct')) as session :
        yield session


