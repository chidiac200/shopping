import sqlalchemy.exc
from sqlmodel import Session, select

class Create:

    @staticmethod
    def add_in_DB(session: Session, table_out, table_in):
        new_customer = table_out.from_orm(table_in)
        session.add(new_customer)
        session.commit()
        session.refresh(new_customer)

        return new_customer

    @staticmethod
    def read_all_data_table(session: Session, table):
        statement = select(table)
        datas = session.exec(statement).all()
        return datas



    #get element in db by id
    @staticmethod
    def read_by_id(session : Session, table : dict, id_table: int):
        product_id = session.get(table, id_table)
        if product_id is None:
            return False
        return product_id
