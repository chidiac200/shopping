from fastapi import FastAPI, Header, Security, Depends
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from typing import Annotated
from routers import customers, products
from database.connexion import create_db_and_tables


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

# this decorateur create all tables in the database
@app.on_event("startup")
def on_startup():
    create_db_and_tables()



origins = ["*"]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
"""
    This section contain all endpoints include in the router
"""
app.include_router(customers.router)
app.include_router(products.router)




